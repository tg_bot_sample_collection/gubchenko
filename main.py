#!venv/bin/python
import logging
from aiogram import Bot, executor, Dispatcher, types
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.state import State, StatesGroup

import handlers

# Объект бота
bot = Bot(token="token")
# Диспетчер для бота
dp = Dispatcher(bot, storage=MemoryStorage())
# Включаем логирование, чтобы не пропустить важные сообщения
logging.basicConfig(level=logging.INFO)

# Создать шаблон
@dp.message_handler(commands="start")
async def cmd_start(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button_1 = types.KeyboardButton(text="Создать заметку")
    keyboard.add(button_1)
    button_2 = "Просмотреть заметку"
    keyboard.add(button_2)
    button_3 = "Удалить заметку"
    keyboard.add(button_3)
    await message.answer("Здесь вы можете сделать себе заметки", reply_markup=keyboard)

my_notes = []

class UserNote (StatesGroup):
    wait_note = State()
    wait_note2 = State()
class UserNoteAns (StatesGroup):
    wait_note = State()
    wait_note2 = State()
class UserNoteDel (StatesGroup):
    wait_note = State()
    wait_note2 = State()

# Хэндлер на команду Создать заметку
async def create_note(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    await message.answer("Введите заметку", reply_markup=types.ReplyKeyboardRemove())
    await UserNote.wait_note.set()
async def note_finished(message: types.Message, state: FSMContext):
    my_notes.append(message.text)
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button_1 = types.KeyboardButton(text="Создать заметку")
    keyboard.add(button_1)
    button_2 = "Просмотреть заметку"
    keyboard.add(button_2)
    button_3 = "Удалить заметку"
    keyboard.add(button_3)
    await message.answer("Заметка создана!", reply_markup=keyboard)
    await state.finish()

# Хэндлер на команду Просмотреть заметку
async def watch_note(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for notes in my_notes:
        keyboard.add(notes)
    await message.answer("Выберите заметку:", reply_markup=keyboard)
    await UserNoteAns.wait_note.set()


# Хэндлер на команду Удалить заметку
async def delete_note(message: types.Message):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    for notes in my_notes:
        keyboard.add(notes)
    await message.answer("Выберите заметку:", reply_markup=keyboard)
    await UserNoteDel.wait_note.set()
async def delete_finished(message: types.Message, state: FSMContext):
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    button_1 = types.KeyboardButton(text="Создать заметку")
    keyboard.add(button_1)
    button_2 = "Просмотреть заметку"
    keyboard.add(button_2)
    button_3 = "Удалить заметку"
    keyboard.add(button_3)
    my_notes.remove(message.text)
    await message.answer("Готово!", reply_markup=keyboard)
    await state.finish()

def register_handlers(dp: Dispatcher):
    dp.register_message_handler(create_note, lambda message: message.text == "Создать заметку", state="*")
    dp.register_message_handler(note_finished, state=UserNote.wait_note)
    dp.register_message_handler(watch_note, lambda message: message.text == "Просмотреть заметку", state="*")
    dp.register_message_handler(handlers.watch_finished, state=UserNoteAns.wait_note)
    dp.register_message_handler(delete_note, lambda message: message.text == "Удалить заметку", state="*")
    dp.register_message_handler(delete_finished, state=UserNoteDel.wait_note)
register_handlers(dp)

if __name__ == "__main__":
    # Запуск бота
    executor.start_polling(dp, skip_updates=True)
